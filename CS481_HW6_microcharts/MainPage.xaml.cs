﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;


namespace CS481_HW6_microcharts
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            CreateCharts();
        }

        private void CreateCharts()
        {
            var entry = new List<Entry>()
            {
                new Entry(210)
                {
                    Label ="2008",
                    Color = SKColor.Parse("#236432"),
                },
                 new Entry(309)
                {
                    Label ="2010",
                    Color = SKColor.Parse("#238432"),
                },
                 new Entry(413)
                {
                    Label ="2012",
                    Color = SKColor.Parse("#232932"),
                },
                 new Entry(822)
                {
                    Label ="2014",
                    Color = SKColor.Parse("#346988"),
                },
                 new Entry(316)
                {
                    Label ="2016",
                    Color = SKColor.Parse("#368958"),
                }

            };

            var barChart = new BarChart() { Entries = entry };
            var donutChart = new DonutChart() { Entries = entry };
            var pointChart = new PointChart() { Entries = entry };
            var lineChart = new LineChart() { Entries = entry };

            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
            Chart3.Chart = pointChart;
            Chart4.Chart = lineChart;

        }

        public void OnSelectedIndex(Object sender, EventArgs e){
            Picker chartspicker = (Picker)sender;
            var chart = chartspicker.SelectedIndex;

            switch (chart)
            {
                case 0:
                    Chart2.IsVisible = false;
                    Chart3.IsVisible = false;
                    Chart4.IsVisible = false;
                    Chart1.IsVisible = true;
                    break;
                case 1:
                    Chart1.IsVisible = false;
                    Chart3.IsVisible = false;
                    Chart4.IsVisible = false;
                    Chart2.IsVisible = true;
                    break;
                case 2:
                    Chart1.IsVisible = false;
                    Chart2.IsVisible = false;
                    Chart4.IsVisible = false;
                    Chart3.IsVisible = true;
                    break;
                case 3:
                    Chart1.IsVisible = false;
                    Chart2.IsVisible = false;
                    Chart3.IsVisible = false;
                    Chart4.IsVisible = true;
                    break;
            }
        }
    }
}
